﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataView
{
    public partial class CreateDialog : Form
    {
        public CreateDialog()
        {
            InitializeComponent();
            // change saveFileDialog's default path
        }

        private void SolveButton_Click(object sender, EventArgs e)
        {
            // 2. use SaveFileDialog
            // 1. grab data from textboxes
            // 3. Write data to file
            //  . Execute one of the binaries
            // 4. Display resulting file's name
        }

        private void TryRandomData(object sender, EventArgs e)
        {
            // put random numbers to textboxes
        }
    }
}
