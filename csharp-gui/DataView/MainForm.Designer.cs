﻿namespace DataView
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.graphViewBox = new System.Windows.Forms.PictureBox();
            this.infoGroupBox = new System.Windows.Forms.GroupBox();
            this.matrixLabel = new System.Windows.Forms.Label();
            this.pathsListBox = new System.Windows.Forms.ListBox();
            this.prevPathButton = new System.Windows.Forms.Button();
            this.nextPathButton = new System.Windows.Forms.Button();
            this.loadFileButton = new System.Windows.Forms.Button();
            this.fileNameTextBox = new System.Windows.Forms.TextBox();
            this.redrawGraphButton = new System.Windows.Forms.Button();
            this.rider1Label = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.graphViewBox)).BeginInit();
            this.infoGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // graphViewBox
            // 
            this.graphViewBox.BackColor = System.Drawing.Color.White;
            this.graphViewBox.Location = new System.Drawing.Point(272, 12);
            this.graphViewBox.Name = "graphViewBox";
            this.graphViewBox.Size = new System.Drawing.Size(800, 700);
            this.graphViewBox.TabIndex = 0;
            this.graphViewBox.TabStop = false;
            this.graphViewBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.graphViewBox_MouseDown);
            this.graphViewBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.graphViewBox_MouseMove);
            // 
            // infoGroupBox
            // 
            this.infoGroupBox.Controls.Add(this.matrixLabel);
            this.infoGroupBox.Location = new System.Drawing.Point(12, 12);
            this.infoGroupBox.Name = "infoGroupBox";
            this.infoGroupBox.Size = new System.Drawing.Size(254, 250);
            this.infoGroupBox.TabIndex = 1;
            this.infoGroupBox.TabStop = false;
            this.infoGroupBox.Text = "Info";
            // 
            // matrixLabel
            // 
            this.matrixLabel.AutoSize = true;
            this.matrixLabel.Location = new System.Drawing.Point(6, 27);
            this.matrixLabel.Name = "matrixLabel";
            this.matrixLabel.Size = new System.Drawing.Size(0, 23);
            this.matrixLabel.TabIndex = 0;
            // 
            // pathsListBox
            // 
            this.pathsListBox.FormattingEnabled = true;
            this.pathsListBox.ItemHeight = 23;
            this.pathsListBox.Location = new System.Drawing.Point(12, 304);
            this.pathsListBox.Name = "pathsListBox";
            this.pathsListBox.Size = new System.Drawing.Size(254, 464);
            this.pathsListBox.TabIndex = 2;
            this.pathsListBox.SelectedIndexChanged += new System.EventHandler(this.pathsListBox_SelectedIndexChanged);
            // 
            // prevPathButton
            // 
            this.prevPathButton.Location = new System.Drawing.Point(12, 265);
            this.prevPathButton.Name = "prevPathButton";
            this.prevPathButton.Size = new System.Drawing.Size(118, 31);
            this.prevPathButton.TabIndex = 3;
            this.prevPathButton.Text = "Rider1";
            this.prevPathButton.UseVisualStyleBackColor = true;
            this.prevPathButton.Click += new System.EventHandler(this.prevPathButton_Click);
            // 
            // nextPathButton
            // 
            this.nextPathButton.Location = new System.Drawing.Point(148, 265);
            this.nextPathButton.Name = "nextPathButton";
            this.nextPathButton.Size = new System.Drawing.Size(118, 31);
            this.nextPathButton.TabIndex = 4;
            this.nextPathButton.Text = "Rider2";
            this.nextPathButton.UseVisualStyleBackColor = true;
            this.nextPathButton.Click += new System.EventHandler(this.nextPathButton_Click);
            // 
            // loadFileButton
            // 
            this.loadFileButton.Location = new System.Drawing.Point(272, 718);
            this.loadFileButton.Name = "loadFileButton";
            this.loadFileButton.Size = new System.Drawing.Size(98, 50);
            this.loadFileButton.TabIndex = 5;
            this.loadFileButton.Text = "Load File";
            this.loadFileButton.UseVisualStyleBackColor = true;
            this.loadFileButton.Click += new System.EventHandler(this.loadFileButton_Click);
            // 
            // fileNameTextBox
            // 
            this.fileNameTextBox.Enabled = false;
            this.fileNameTextBox.Location = new System.Drawing.Point(617, 46);
            this.fileNameTextBox.Name = "fileNameTextBox";
            this.fileNameTextBox.Size = new System.Drawing.Size(228, 31);
            this.fileNameTextBox.TabIndex = 6;
            this.fileNameTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.fileNameTextBox.Visible = false;
            // 
            // redrawGraphButton
            // 
            this.redrawGraphButton.Location = new System.Drawing.Point(943, 718);
            this.redrawGraphButton.Name = "redrawGraphButton";
            this.redrawGraphButton.Size = new System.Drawing.Size(129, 50);
            this.redrawGraphButton.TabIndex = 7;
            this.redrawGraphButton.Text = "Change layout";
            this.redrawGraphButton.UseVisualStyleBackColor = true;
            this.redrawGraphButton.Click += new System.EventHandler(this.redrawGraphButton_Click);
            // 
            // rider1Label
            // 
            this.rider1Label.AutoSize = true;
            this.rider1Label.Enabled = false;
            this.rider1Label.Location = new System.Drawing.Point(402, 48);
            this.rider1Label.Name = "rider1Label";
            this.rider1Label.Size = new System.Drawing.Size(60, 23);
            this.rider1Label.TabIndex = 8;
            this.rider1Label.Text = "Rider1";
            this.rider1Label.Visible = false;
            this.rider1Label.MouseClick += new System.Windows.Forms.MouseEventHandler(this.rider1Label_MouseClick);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(304, 39);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(92, 40);
            this.button1.TabIndex = 9;
            this.button1.Text = "add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(851, 40);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(117, 41);
            this.button2.TabIndex = 10;
            this.button2.Text = "upload";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(481, 40);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(107, 39);
            this.button3.TabIndex = 11;
            this.button3.Text = "download";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 782);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.rider1Label);
            this.Controls.Add(this.redrawGraphButton);
            this.Controls.Add(this.fileNameTextBox);
            this.Controls.Add(this.loadFileButton);
            this.Controls.Add(this.nextPathButton);
            this.Controls.Add(this.prevPathButton);
            this.Controls.Add(this.pathsListBox);
            this.Controls.Add(this.infoGroupBox);
            this.Controls.Add(this.graphViewBox);
            this.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "MainForm";
            this.Text = "DataView";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.graphViewBox)).EndInit();
            this.infoGroupBox.ResumeLayout(false);
            this.infoGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox graphViewBox;
        private System.Windows.Forms.GroupBox infoGroupBox;
        private System.Windows.Forms.ListBox pathsListBox;
        private System.Windows.Forms.Button prevPathButton;
        private System.Windows.Forms.Button nextPathButton;
        private System.Windows.Forms.Button loadFileButton;
        private System.Windows.Forms.TextBox fileNameTextBox;
        private System.Windows.Forms.Button redrawGraphButton;
        private System.Windows.Forms.Label rider1Label;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label matrixLabel;
    }
}

