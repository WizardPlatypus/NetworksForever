﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.IO;

namespace DataView
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        Point[] cities;
        Rider[] riders;
        int[,] matrix;

        Graphics canvas;
        Bitmap image;
        Color bg_color;

        Random rnd;

        Brush dot_brush;
        Pen line_pen;

        List<Point[]> layouts;
        int index_layout = 0;
        int currentRider = 0;

        double criteria;
        const float amplitude = 8f;

        private void redrawGraphButton_Click(object sender, EventArgs e)
        {
            cities = layouts[index_layout++];
            index_layout %= layouts.Count;
            drawItAll();
            graphViewBox.Image = image;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            image = new Bitmap(graphViewBox.Width, graphViewBox.Height);
            canvas = Graphics.FromImage(image);
            bg_color = Color.White;
            line_pen = new Pen(Color.Green, 2f);
            dot_brush = new SolidBrush(Color.Gray);

            cities = new Point[4];
            riders = new Rider[2];

            criteria = Math.Sqrt(image.Width * image.Height) / cities.Length;
            rnd = new Random();

            //pathsListBox.SelectedIndex = 1;
            //for (int i = 0; i < riders.Length; ++i)
            //    riders[i].ChangePath(pathsListBox.SelectedIndex);

            layouts = new List<Point[]>();
            layouts.Add(new Point[]
            {
                new Point(529, 650),
                new Point(731, 78),
                new Point(188, 138),
                new Point(68, 513),
            });
            layouts.Add(new Point[]
            {
                new Point(400, 373),
                new Point(738, 537),
                new Point(372, 29),
                new Point(30, 622)
            });
            layouts.Add(new Point[]
            {
                new Point(674, 583),
                new Point(736, 138),
                new Point(272, 59),
                new Point(31, 611),
            });
            layouts.Add(new Point[]
            {
                new Point(545, 675),
                new Point(755, 42),
                new Point(405, 295),
                new Point(31, 156),
            });
            cities = layouts[index_layout++];

            COLORS = new Color[]
            {
                //Color.FromArgb(255, 0, 31, 63), // Navy
                Color.FromArgb(255, 0, 116, 217), // Blue
                //Color.FromArgb(255, 127, 219, 255), // Aqua
                //Color.FromArgb(255, 57, 204, 204), // Teal
                Color.FromArgb(255, 61, 153, 112), // Olive
                //Color.FromArgb(255, 46, 204, 64), // Green
                //Color.FromArgb(255, 1, 255, 112), // Lime
                //Color.FromArgb(255, 255, 220, 0), // Yellow
                Color.FromArgb(255, 255, 133, 27), // Orange
                //Color.FromArgb(255, 255, 65, 54), // Red
                Color.FromArgb(255, 133, 20, 70), // Maroon
                Color.FromArgb(255, 240, 18, 190), // Fuchsia
                Color.FromArgb(255, 177, 13, 201) // Purple
            };
            CHECKS = new bool[COLORS.Length];

        }

        ///////////////////////////
        // COMMON LOAD FROM FILE // #ACCEPTABLE
        ///////////////////////////

        private void loadFileButton_Click(object sender, EventArgs e)
        {
            var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = Application.StartupPath;
                openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filePath = openFileDialog.FileName;
                }
                else
                    return;
            }
            StreamReader sr = new StreamReader(filePath);

            string line;
            string[] indexes, positions;

            matrixLabel.Text = "";

            /// Origin
            int[] origin = new int[2];
            line = sr.ReadLine();
            matrixLabel.Text += "Origin: " + line + "\n";
            indexes = line.Split(' ');
            origin[0] = int.Parse(indexes[0]);
            origin[1] = int.Parse(indexes[1]);

            /// Number of paths
            int count = int.Parse(sr.ReadLine());
            matrixLabel.Text += "Win count: " + count.ToString() + "\n";

            /// Paths
            riders = new Rider[2];
            for (int i = 0; i < riders.Length; ++i)
                riders[i] = new Rider();
            List<int[]> path1s = new List<int[]>(), path2s = new List<int[]>();
            List<int> path1 = new List<int>(), path2 = new List<int>();
            pathsListBox.Items.Clear();
            for (int i = 0; i < count; ++i)
            {
                line = sr.ReadLine();
                pathsListBox.Items.Add(origin[0].ToString() + " " + origin[1].ToString() + ";" + line);
                positions = line.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                
                path1.Add(origin[0]);
                path2.Add(origin[1]);
                foreach (string position in positions)
                {
                    indexes = position.Split(' ');
                    int index = int.Parse(indexes[0]);
                    if (path1.Last() != index)
                        path1.Add(index);
                    index = int.Parse(indexes[1]);
                    if (path2.Last() != index)
                        path2.Add(index);
                }
                path1s.Add(path1.ToArray());
                path2s.Add(path2.ToArray());
                path1.Clear();
                path2.Clear();
            }           

            /// Time
            int time = int.Parse(sr.ReadLine());
            matrixLabel.Text += "Time: " + time.ToString() + "\n";

            /// Matrix
            matrix = new int[4, 4];
            matrixLabel.Text += "Matrix:\n";
            for (int i = 0; i < 4; ++i)
            {
                line = sr.ReadLine();
                matrixLabel.Text += line + "\n";
                string[] data = line.Split(' ');
                for (int j = 0; j < 4; ++j)
                {
                    matrix[i, j] = int.Parse(data[j]);
                }
            }


            riders[0].paths = path1s;
            riders[1].paths = path2s;
            riders[0].ChangeColor(Color.Blue);
            riders[1].ChangeColor(Color.Red);
            pathsListBox.SelectedIndex = 0;
            riders[0].ChangePath(pathsListBox.SelectedIndex);
            riders[1].ChangePath(pathsListBox.SelectedIndex);

            currentRider = 0;
        }

        ////////////////////
        // MOUSE HANDLING // #ACCEPTABLE
        ////////////////////
        int INDEX;
        private void graphViewBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
                return;
            if (INDEX < cities.Length)
            {
                cities[INDEX] = e.Location;
            }
            drawItAll();
            graphViewBox.Image = image;
        }
        private void graphViewBox_MouseDown(object sender, MouseEventArgs e)
        {
            for (INDEX = 0; INDEX < cities.Length; ++INDEX)
            {
                if (distance(e.Location, cities[INDEX]) < 8)
                {
                    break;
                }
            }
        }
        private void pathsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < riders.Length; ++i)
            {
                riders[i].ChangePath(pathsListBox.SelectedIndex);
            }
            drawItAll();
            graphViewBox.Image = image;
        }

        ////////////////////
        // HELP FUNCTIONS //
        ////////////////////
        double distance(Point a, Point b)
        {
            int dx = b.X - a.X;
            int dy = b.Y - a.Y;
            return Math.Sqrt(dx * dx + dy * dy);
        }
        private static Color[] COLORS;
        private static bool[] CHECKS;
        public static Color GetRandomrColor(Random rnd)
        {
            //var rnd = new Random();
            int index = 0;
            do
            {
                index = rnd.Next(0, COLORS.Length);
            } while (CHECKS[index]);
            CHECKS[index] = true;
            if (CHECKS.All(a => a))
                CHECKS.Initialize(); // binds to default
            return COLORS[index];

        }
        void makeDot(Point point)
        {
            canvas.FillEllipse(dot_brush, point.X - amplitude, point.Y - amplitude, amplitude * 2, amplitude * 2);
        }
        void drawItAll()
        {
            canvas.Clear(bg_color);

            for (int i = 0; i < cities.Length; ++i)
            {
                for (int j = i + 1; j < cities.Length; ++j)
                {
                    canvas.DrawLine(line_pen, cities[i], cities[j]);
                    //Point middle = new Point((cities[i].X + cities[j].X) / 2, (cities[i].Y + cities[j].Y) / 2);
                    //canvas.DrawString(
                    //    matrix[i, j].ToString(),
                    //    new Font("Verdana", 17),
                    //    Brushes.Black, middle.X - 14, middle.Y - 14
                    //    );
                }
            }
            if (riders[currentRider] != null)
                riders[currentRider].Draw(canvas, cities, matrix);
            for (int i = 0; i < cities.Length; ++i)
            {
                var victim = cities[i];
                canvas.FillEllipse(Brushes.White, victim.X - 10, victim.Y - 12, 24, 24);
                canvas.DrawEllipse(Pens.Black, victim.X - 10, victim.Y - 12, 24, 24);
                canvas.DrawString(
                    i.ToString(),
                    new Font("Verdana", 16),
                    Brushes.Black, victim.X - 9, victim.Y - 12
                    );
            }
        }


        //#######//
        // TRASH //
        //#######//
        private void button3_Click(object sender, EventArgs e)
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = Application.StartupPath;
                openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;
                }
            }
            List<Point> points = new List<Point>();
            StreamReader sr = new StreamReader(filePath);
            while (!sr.EndOfStream)
            {
                string s = sr.ReadLine();
                string[] data = s.Split(new char[] { '.', ';' }, StringSplitOptions.RemoveEmptyEntries);
                points.Add(new Point(int.Parse(data[0]), int.Parse(data[1])));
            }
            sr.Close();
            cities = points.ToArray();
            drawItAll();
            graphViewBox.Image = image;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            StreamWriter sw = new StreamWriter("points.txt", false);
            foreach (var point in cities)
            {
                sw.WriteLine(point.X + "; " + point.Y);
            }
            sw.Close();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            drawItAll();
            graphViewBox.Image = image;
        }
        private void rider1Label_MouseClick(object sender, MouseEventArgs e)
        {
        }

        private void prevPathButton_Click(object sender, EventArgs e)
        {
            currentRider = 0;
            drawItAll();
            graphViewBox.Image = image;
        }

        private void nextPathButton_Click(object sender, EventArgs e)
        {
            currentRider = 1;
            drawItAll();
            graphViewBox.Image = image;
        }
    }
}
