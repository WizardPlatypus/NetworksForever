﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace DataView
{
    class Rider
    {
        private Pen Peter;
        public List<int[]> paths;
        public int current_path;
        private const float WIDTH = 3f;

        public Rider()
        {
            Peter = new Pen(Color.DarkGreen, WIDTH);
            Peter.CustomEndCap = new System.Drawing.Drawing2D.AdjustableArrowCap(4, 20, true);
            paths = new List<int[]>();
            current_path = 0;
        }

        public void ChangeColor(Color color)
        {
            Peter.Color = color;
        }

        public void ChangePath(int index)
        {
            current_path = index;
        }
        
        public Rider(List<int[]> paths, int current_path, Color color)
        {
            Peter = new Pen(color, WIDTH);
            Peter.CustomEndCap = new System.Drawing.Drawing2D.AdjustableArrowCap(4, 20, true);
            this.paths = paths;
            this.current_path = current_path;
        }

        public void Draw(Graphics g, Point[] source, int[,] matrix)
        {
            for (int i = 0; i < paths[current_path].Length- 1; ++i)
            {
                int from = paths[current_path][i], to = paths[current_path][i + 1];
                Point begin = source[from],
                    end = source[to],
                    middle = magicPoint(begin, end, 0.2);
                g.DrawCurve(Peter, new Point[] { begin, middle, end });
                g.FillRectangle(Brushes.White, middle.X - 24, middle.Y - 20, 48, 24);
                g.DrawRectangle(Pens.Black, middle.X - 24, middle.Y - 20, 48, 24);
                g.DrawString(
                    (i + 1).ToString() + ":" + matrix[from, to].ToString(),
                    new Font("Verdana", 16),
                    Brushes.Black, middle.X - 20, middle.Y - 20
                    );
            }
        }

        static Point magicPoint(Point A, Point B, double k)
        {
            Point O = new Point((A.X + B.X) / 2, (A.Y + B.Y) / 2);
            return new Point(
                O.X - (int)(k * (B.Y - O.Y)),
                O.Y + (int)(k * (B.X - O.X))
                );
        }
    }
}
