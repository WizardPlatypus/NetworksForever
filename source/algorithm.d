module algorithm;

  private:
import std.algorithm.sorting : sort;
static import Data;
import moment;
import distributor;
import riders;
debug import std.stdio;

/** Returns sorted numbers chosen from input by given indexes. */
uint[] hash( uint[] input, uint[] indexes ) {
    uint[] arr;
    arr.length = indexes.length;
    for ( uint i = 0; i < indexes.length; ++i ) {
        arr[i] = input[indexes[i]];
    }
    sort( arr );
    return arr;
}

/** Searches given input in data by indexes stored in groups. */
bool match( uint[][] data, uint[] input, uint[][] groups ) {
    bool matches;
    foreach ( indexes; groups ) {
        matches = false;
        foreach ( element; data ) {
            matches = hash( element, indexes ) == hash( input, indexes );
            if ( matches )
                return true;
        }
    }
    return false;
}

/**
New step for riders. [probably long explanation]
Params:
    then = previous moment.
    riders = riders, who will move the whole game.
    visits = array, constains global visits of cities.
    T = time passed.
*/
void step(Moment then, Rider[] riders, bool[] visits, uint T)
{
    Moment now = new Moment( riders.from, riders.to, then ); // works fine
    T += riders.move( visits );
    if ( Data.ItIsVersion( Data.Versions.Opt2 ) ) {
        if ( T > Data.Time ) {
            ++Data.stoppedCount;
            return;
        }
    }
    if ( visits.conjunction ) {
        Data.handle( T, now.past );
        return;
    }
    if ( riders.stuck ) {
        ++Data.failedCount;
        return;
    }
    auto d = new Distributor();
    foreach(rider; riders)
        d.add(rider.perspectives());
    if ( Data.ItIsVersion( Data.Versions.Opt1 ) ) {
        uint[][] arrs = riders.groups.dup;
        uint[][] data;
        foreach ( combination; d ) {
            if ( !data.match( combination, arrs ) ) {
                step( now, riders.dup.apply(combination), visits.dup, T );
                data ~= combination.dup;
            }
        }
    } else {
        foreach ( combination; d ) {
            step( now, riders.dup.apply( combination ), visits.dup, T );
        }
    }
}

  public:
/** Starts algorithm **/
void start( uint[][] input, uint[] origin )
in ( origin.length == Data.ridersNumber )
in {
    assert( input.length == Data.citiesNumber );
    foreach ( row; input ) {
        assert( row.length == Data.citiesNumber );
    }
}
do {
    Data.origin = origin;
    Data.matrix = input;
    // Setting riders
    Rider[] riders;
    riders.length = Data.ridersNumber;
    for (uint i = 0; i < Data.ridersNumber; ++i)
        riders[i] = Rider(origin[i]);
    // Setting visits
    bool[] visits;
    visits.length = Data.citiesNumber;
    visits[] = false;
    for (uint i = 0; i < Data.ridersNumber; ++i)
        visits[origin[i]] = true;
    // Creating combinations generator
    auto generator = new Distributor( );
    foreach(rider; riders)
        generator.add( rider.perspectives( ) );
    if ( Data.ItIsVersion( Data.Versions.Opt1 ) ) {
        // optimazing combinations
        uint[][] arrs = riders.groups.dup;
        uint[][] data;
        foreach ( combination; generator ) {
            if ( !data.match( combination, arrs ) ) {
                step( null, riders.dup.apply( combination ), visits.dup, 0 );
                data ~= combination.dup;
            }
        }
    } else {
        foreach ( combination; generator ) {
            step( null, riders.dup.apply( combination ), visits.dup, 0 );
        }
    }
}

