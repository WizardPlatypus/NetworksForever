module Data;

private:

import std.conv : to;

public:

/** Version of algorithm */
enum Versions: uint { None = 0b00, Opt1 = 0b01, Opt2 = 0b10 }
Versions currentVersion = Versions.None;
bool ItIsVersion( Versions suggestion ) {
    return ( currentVersion & suggestion ) == suggestion;
}

/** Number of cities */
immutable uint citiesNumber = 4;
/** Number of riders */
immutable uint ridersNumber = 2;

/** Adjency matrix */
uint[][] matrix;
/** Indexes from where riders started */
uint[] origin;

/** Best time */
uint Time = uint.max;
/** Best paths */
string[] paths = [];

/// Counts ///
uint completeCount = 0;
uint failedCount = 0;
uint winCount = 0;
uint stoppedCount = 0;

void handle( uint newTime, string newPath ) {
    ++completeCount;
    if ( newTime < Time ) {
        Time = newTime;
        destroy( paths );
        paths = [newPath];
        winCount = 1;
    } else if ( newTime == Time ) {
        paths ~= newPath;
        ++winCount;
    }
}

string format( string[] flags ) {
    string res = "";
    foreach ( flag; flags ) {
      switch ( flag ) {
        case "o", "origin":
          foreach ( index; origin )
            res ~= to!string( index ) ~ " ";
          res ~= '\n';
          break;
        case "t", "time":
          res ~= to!string( Time ) ~ '\n';
          break;
        case "m", "matrix":
          foreach ( row; matrix ) {
            foreach ( value; row )
              res ~= to!string( value ) ~ ' ';
            res ~= '\n';
          }
          break;
        case "p", "paths":
          foreach ( path; paths )
              res ~= path ~ '\n';
          break;
        case "c", "count":
          res ~= to!string( winCount ) ~ '\n';
          break;
        case "f", "failed":
          res ~= to!string( failedCount ) ~ '\n';
          break;
        case "d", "done":
          res ~= to!string( completeCount ) ~ '\n';
          break;
        default:
          break;
      }
    }
    return res;
}
