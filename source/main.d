module main;

static import Data;
import algorithm;
import std.stdio;

void eat( File file, ref uint[] origin, ref uint[][] arr )
out {
    assert(origin.length == Data.ridersNumber);
    assert(arr.length == Data.citiesNumber);
    for ( uint i = 0; i < Data.citiesNumber; ++i ) {
        assert(arr[i].length==Data.citiesNumber);
    }
}
do {
    // allocating memory
    origin.length = Data.ridersNumber;
    arr.length = Data.citiesNumber;
    for ( uint i = 0; i < Data.citiesNumber; ++i ) {
        arr[i] = new uint[Data.citiesNumber];
    }
    // using input
    for ( uint i = 0; i < Data.ridersNumber; ++i )
        file.readf( " %d", origin[i] );
    for ( uint i = 0; i < Data.citiesNumber; ++i ) {
        for ( uint j = 0; j < Data.citiesNumber; ++j ) {
            file.readf( " %d", arr[i][j] );
        }
    }
}



int main( string[] args )
{
    import std.algorithm.searching : findSkip;
    import std.array : split;

    foreach ( arg; args ) {
        switch ( arg[0] ) {
            case 'v':
                if ( findSkip( arg, "=" ) ) {
                    Data.currentVersion = Data.Versions.None;
                    foreach ( opt; arg.split( "," ) ) {
                        switch ( opt ) {
                            case "opt1":
                                Data.currentVersion |= Data.Versions.Opt1;
                                break;
                            case "opt2":
                                Data.currentVersion |= Data.Versions.Opt2;
                                break;
                            default:
                                break;
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    uint[] origin;
    uint[][] arr;
    stdin.eat( origin, arr );

    algorithm.start( arr, origin );

    /* the flags are following:
       o - Origin
       c - Count of best paths
       p - best Paths
       t - best Time
       m - adjency Matrix
    */
    auto data = Data.format( ["o", "c", "p", "t", "m"] );
    write( data );

    return 0;
}

