module moment;

static import Data;
import std.conv : to;

debug import std.stdio;

public:
/** Moment in time **/
class Moment
{
  public:
    /** where riders already are at the current moment */
    uint[] halts, targets;
    /** previous moment */
    Moment then;

    /** Basic constructor */
    this(uint[] halts, uint[] targets )
    in( targets.length == Data.ridersNumber )
    in( halts.length == Data.ridersNumber )
    do {
        this.halts = halts.dup;
        this.targets = targets.dup;
        this.then = null;
    }

    /** Full constructor */
    this(uint[] halts, uint[] targets, Moment then)
    in( targets.length == Data.ridersNumber )
    in( halts.length == Data.ridersNumber )
    do {
        this.halts = halts.dup;
        this.targets = targets.dup;
        this.then = then;
    }

    /**
    Stores info about current moment into a string.

    Returns:
        info about current moment.
    */
    string present() {
        string story = "";
        for ( int i = 0; i < Data.ridersNumber; ++i ) {
            uint from = halts[i], target = targets[i];
            story ~= to!string( from ) ~ '>' ~ to!string( target ) ~ ", ";
        }
        story = to!string(story[0 .. $ - 2].dup) ~ "; ";
        return story;
    }

    /**
    Stores info about current moment's past into a string.

    Returns:
        info about current moment's past.
    */
    string past() {
        if (then !is null)
            return then.past ~ present;
        else
            return present;
    }
}

