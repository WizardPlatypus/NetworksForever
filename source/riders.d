module riders;

private:
static import Data;
alias ArrayRider = Rider[Data.ridersNumber];
alias ArrayBool = bool[Data.citiesNumber];

public:
/** Rider struct */
struct Rider
{
  private:
    alias MatrixBool = bool[Data.citiesNumber][Data.citiesNumber];
  public:
    /** from and where rider is going, and how much time he needs */
    uint from, to, time;
    /** matrix of visited roads */
    MatrixBool visits;
    /** status */
    bool stuck;

    /** constructor */
    this(uint origin)
    in (origin < Data.citiesNumber)
    do {
        this.to = origin;
        this.from = origin;
        this.time = 0;
        visits = new bool[Data.citiesNumber][Data.citiesNumber];
        for (uint i = 0; i < Data.citiesNumber; ++i)
        {
            for (uint j = 0; j < Data.citiesNumber; ++j)
            {
                if (i == j)
                    visits[i][j] = true;
                else
                    visits[i][j] = false;
            }
        }
        stuck = false;
    }

    /**
    Finds perspective targets for this particular rider using
    'visits' matrix.
    If time is not equal to 0, then we return current target.
    Then, if there are some possible roads for this rider,
    we return them, otherwise, rider is stuck, and we return
    value of 'from' field.
    If rider is already stuck, then we just return 'from' field.

    Returns:
        Array of indexes.
    */
    uint[] perspectives()
    {
        if (stuck)
            return [from];
        if (time != 0)
            return [to];
        uint[] indexes;
        for (uint i = 0; i < Data.citiesNumber; ++i)
            if (!visits[from][i])
                indexes ~= i;
        if (indexes.length == 0) {
            stuck = true;
            return [from];
        }
        return indexes;
    }
}

/**
Moves each rider in a given array on suitable(proper) time.
Does not try to catch zeros or smth like that.

Params:
    riders = array of riders, who wille be moved.

Returns:
    proper time, on wich riders were moved.
*/
uint move(ref Rider[] riders, ref bool[] cities)
in( riders.length == Data.ridersNumber )
in( cities.length == Data.citiesNumber )
in {
    foreach (rider; riders) {
        if (!rider.stuck)
            assert(rider.time != 0);
    }
} do {
    uint min = uint.max;
    foreach (rider; riders) {
        if (rider.stuck)
            continue;
        if (rider.time < min)
            min = rider.time;
    }
    foreach (ref rider; riders)
        if (!rider.stuck)
        {
            rider.time -= min;
            if (rider.time == 0)
            {
                rider.visits[rider.from][rider.to] = true;
                cities[rider.to] = true;
                rider.from = rider.to;
            }
        }
    return min;
}

/**
Stores 'from' field of each rider into a new array.
Params:
    riders = array of riders, whos fields will be stored.
Returns:
    array of constant length 'ridersNumber', which contains
    'from' field of each rider.
*/
uint[] from( const ref Rider[] riders )
in (riders.length == Data.ridersNumber)
out(_){ assert(_.length == Data.ridersNumber); }
do {
    uint[] array;
    array.length = Data.ridersNumber;
    for (uint i = 0; i < Data.ridersNumber; ++i)
        array[i] = riders[i].from;
    return array;
}

/**
Stores 'to' field of each rider into a new array.
Params:
    riders = array of riders, whos fields will be stored.
Returns:
    array of constant length 'ridersNumber', which contains
    'to' field of each rider.
*/
uint[] to(const ref Rider[] riders)
in (riders.length == Data.ridersNumber)
out(_){ assert(_.length == Data.ridersNumber); }
do {
    uint[] array;
    array.length = Data.ridersNumber;
    for (uint i = 0; i < Data.ridersNumber; ++i)
        array[i] = riders[i].to;
    return array;
}

/**
Checks whether every rider of a given array is stuck.
Params:
    riders = riders who will be checked.
Returns:
    'true' if every rider is stuck, 'false'
    otherwise.
*/
bool stuck(const ref Rider[] riders) {
    foreach (rider; riders)
        if (!rider.stuck)
            return false;
    return true;
}

/**
Stores 'time' field of each rider into a new array.
Params:
    riders = array of riders, whos fields will be stored.
Returns:
    array of constant length 'ridersNumber', which contains
    'time' field of each rider.
*/
uint[] time(const ref Rider[] riders)
in (riders.length == Data.ridersNumber)
out(_){ assert (_.length == Data.ridersNumber); }
do {
    uint[] array;
    array.length = Data.ridersNumber;
    for (uint i = 0; i < Data.ridersNumber; ++i)
        array[i] = riders[i].time;
    return array;
}

/**
Checks whether every element of a given array is equal
    to 'true'.
Params:
    array = array which contanis elements to be checked.
Returns:
    'true' if every element is equal to 'true', 'false'
    otherwise.
*/
bool conjunction(bool[] array) {
    foreach (element; array)
        if (!element)
            return false;
    return true;
}

/**
Applies changes to array of riders.
Params:
    riders = array of riders, which will be changed.
    changes = changes to be applyed, indexes of new
    targets.
Returns:
    changed array.
*/
Rider[] apply( Rider[] riders, uint[] changes)
in ( riders.length == changes.length )
out (_) { assert( _.length == Data.ridersNumber ); }
do {
    for (uint i = 0; i < Data.ridersNumber; ++i)
    {
        if (riders[i].to != changes[i])
        {
            riders[i].to = changes[i];
            riders[i].time = Data.matrix[riders[i].from][riders[i].to];
        }
    }
    return riders;
}

uint[][] groups( const Rider[] riders ) {
    uint[][] arrs;
    for ( uint i = 0; i < Data.ridersNumber; ++i ) {
        uint[] arr = [ i ];
        for ( uint j = i + 1; j < Data.ridersNumber; ++j ) {
            if ( riders[i] == riders[j] ) {
                arr ~= j;
            }
        }
        if ( arr.length != 1 )
            arrs ~= arr;
    }
    return arrs;
}
